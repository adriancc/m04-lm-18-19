# practica 1
 
1) **Consulta les pàgines de manual ascii(7), latin1(7), utf8(7).**  
man ascii 7 muestra la descripción del set de caracteres ASCII y una lista con los caracteres incluidos y su numeración 
en octal, decimal y hexadecimal.  
man latin1 7 muestra la descripción del set de caracteres ISO 8859-1 (Latin1) junto con una lista con los caracteres incluidos 
y su numeración en octal, decimal y hexadecimal. También incluye un listado de otras codificaciones 
incluidas en el mismo estandard.  
man utf-8 7 muestra la descripción y propiedades del set de caracteres utf-8, así como ejemplos de cómo se codifican los 
caracteres con este sistema de codificación.
  
1) **Compara el resultat de les ordres man passwd i man 5 passwd.**  
man passwd muestra la ayuda del comando passwd (para cambiar claves de usuario). man 5 passwd muestra la ayuda para el
archivo de configuración /etc/passwd (que almacena información sobre los usuarios del sistema)  

1) **Verifica que entens tots els exemples anteriors de redirecció del shell.**  
`cmd > output` redirige la salida del comando al archivo output  
`cmd < input` ejecuta el comando utilizando input como datos de entrada  
`cmd > output < input ` ejecuta el comando utilizando input como datos de entrada y redirige la salida del 
comando al archivo output    
`> fitxer` crea un fichero en blanco  
`cmd > /dev/null 2>&1` redirige la salida del comando a /dev/null, y el error estandar al mismo sitio que se 
ha redirigido la salida (/dev/null)  
`cmd 2>&1 | less` redirige el error estandard del comando al mismo sitio que apunte la salida estandard, y a 
continuación alimenta el comando less con toda la salida (incluidos errores) del primer comando.  

1) **Converteix un fitxer existent en el sistema a finals de línia propis de Windows.**  
`unix2dos file`  
Si miramos el tamaño del archivo antes y después del comando vemos que al pasar al formato windows el archivo aumenta 
su tamaño.

1) **Troba cadenes de text dins de fitxers binaris.**  
`strings file` busca cadenas de caracteres imprimibles en un binario. Si concatenamos con un grep podemos buscar un 
texto concreto.  

1) **Converteix la codificació de fitxers de text entre les codificacions UTF-8 i latin1.**  
`iconv -f utf-8 -t iso8859-1 docunix.txt -o docunix2.txt`

1) **Practica els tutorials citats en la llista d’enllaços.**  

