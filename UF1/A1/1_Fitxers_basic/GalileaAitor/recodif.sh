#! /bin/bash
# @edt ASIX M04 2018-19
# Aitor Galilea ISX78745231
# Programa que recibe por parametro el nombre de un fichero de texto plano y modifica su formato,
# si era formato windows lo pasa a linux y si era formato linux lo pasa a windows. Por ultimo guarda
# el archivo modificado con el nombre que se le suministra como segundo parametro
# synopsis: prog filein fileout
#----------------------------
ERR_NARGS=1
ERR_FILE=2

#control de parametros
if [ "$#" -ne 2 ]
then
  echo "numero de parametros incorrecto" >/dev/stderr
  echo "usage: recodif filein fileout" >/dev/stderr
  exit "$ERR_NARGS"
fi

filein="$1"
fileout="$2"
#echo "$filein"
#echo "$fileout"

#filein existe
if ! [ -f "$filein" ]
then
  echo "el argumento no es un nombre de archivo valido">/dev/stderr
  echo "usage: recodif filein fileout">/dev/stderr
  exit "$ERR_FILE"
fi

#comprobamos el formato del archivo y pasamos al otro
grep -r $'\r' "$filein" &> /dev/null
#si el doc tiene formato linux (no tiene simbolos CR)
if [ "$?" -ne 0 ]
then
  unix2dos -n "$filein" "$fileout" &>/dev/null
  echo "$filein convertido a formato windows y guardado como $fileout"
#si el doc tiene formato windows (tiene simbolos CR)
else
  dos2unix -n "$filein" "$fileout" &>/dev/null
  echo "$filein convertido a formato unix y guardado como $fileout"
fi
exit "$err"