#!/bin/bash
#Daniel Cano ASIX-M01 2017-2018
#11/01/2018
#Descripcio: Validar que s'han rebut dos args
#Synpsis: prog format1 format2 [dir|file]
###############################################

ERR_NARG=1
OK=0
#Validar args
if [ $# -lt 2 -o $# -gt 3 ]
then
  echo "err: nombre d'arguments incorrecte"
  echo "prog format1 format2 [dir|file]"
  exit $ERR_NARG
fi

#Fer xixa
format1=$1
format2=$2
if [ $# -eq 3 ] 
then
  tipus=$3
  if [ -f $tipus ]
  then 
    iconv -f $format1 -t $format2 $tipus -o /dev/stdout
  elif [ -d $tipus ]
  then
    elements=$(ls $tipus)
  else
    echo "error: $3 no es ni un file ni un directori"
    exit 2
  fi
else
  elements=$(ls)
fi

for element in $elements
do
  if [ -f $tipus/$element ]
  then
    echo "$element:"
    iconv -f $format1 -t $format2 $tipus/$element -o /dev/stdout | sed -r 's/(.*)/  \1/g'
  else
    echo "$element:"
     echo -e "\t $element no es un file"
  fi
done
