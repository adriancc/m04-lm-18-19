## Ordres:
##### dos2unix/unix2dos: convierte archivos dedos a unix/linux y viceversa.
    dos2unix/unix2dos [options] [file ...] [-n infile outfile ...]

##### iconv/convmv: convert text from one character encoding to another
	iconv [options] [-f from-encoding] [-t to-encoding] [inputfile]...
	convmv [options] FILE(S) ... DIRECTORY(S)
		
##### strings: print the strings of printable characters in files.
	strings file
	
##### od: dump files in octal and other formats
	od [OPTION]... [FILE]...

##### hexdump: display file contents in hexadecimal, decimal, octal, or ascii
	hexdump [options] file...

## Exercicis:

##### Compara el resultat de les ordres man passwd i man 5 passwd.
	Man passwd es refereix a l'rodre "passwd" i man 5 passwd es refereix al fitxer /etc/passwd
		
##### Converteix la codificació de fitxers de text entre les codificacions UTF-8 i latin1.
	iconv -f UTF-8 -t LATIN1 /tmp file.txt

## Treballat a classe:
    > dnf -y install retext pandoc meld texlive
    1. `pandoc`: serveix per pasar un file a markdown
        exemple: pandoc -f html -t markdown index.html
    2. meld: per comparar files en *mode grafic*
        exemple: meld file1 file2

## Clau ssh
1. generem la clau ssh --> ssh-keygen -o -t rsa -C        "isx53320079@i11.informatica.escoladeltreball.org" -b 4096
2. veiem la clau ssh --> cat ~/.ssh/id_rsa.pub
3. canviem de http a ssh --> ssh -T git@gitlab.com
    1. start the ssh-agent in the background (si sale este error: **Permission denied (publickey)**)
        1. $ eval "$(ssh-agent -s)"

        2. $ ssh-add 
    2. ssh -T git@gitlab.com
        * `Welcome to GitLab, @isx53320079!` --> funciona bien

    1. per comprobar si ha funcionat --> git remote -v
    ```
       ha de sortir aixo:
       origin git@gitlab.com:isx53320079/m04-lm-18-19.git (fetch)
       origin	git@gitlab.com:isx53320079/m04-lm-18-19.git (push)
	```
5. sino funciona borrem i l'afegim de nou amb la clau ssh
    1. git remote remove origin 
  	2. git remote add origin git@gitlab.com:isx53320079/m04-lm-18-19.git
    3. per comprobar si ha funcionat --> git remote -v  
    
    ```
        ha de sortir aixo:
        origin git@gitlab.com:isx53320079/m04-lm-18-19.git (fetch)
        origin	git@gitlab.com:isx53320079/m04-lm-18-19.git (push)
	```
    
## Add remote from original repository in your forked repository:
	cd into/cloned/fork-repo
    git remote add m04 git@gitlab.com:jandugar/m04-lm-18-19.git
    git fetch m04
    git pull m04 master
	
