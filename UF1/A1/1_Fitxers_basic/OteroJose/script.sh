#!/bin/bash

#salidas
error=1
ok=0
##Gestion de errores
if [ $# -ne 3 -o $# == "--help" ]
then
  echo "ussage: directorio, input format, output format"
  exit $error
fi
#########################
#variables
directorio=$1
input=$2
output=$3
#########################
#SCRIPT
lista=$(ls $directorio)
cd $1 &> /dev/null
for item in $lista
do
  iconv -f $input -t $output $item -o $item &> /dev/null
done
exit $ok
  
