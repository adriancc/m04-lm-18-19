## GIT CONFIGURATION

 <hr />

### Insta·lar el git

> dnf -y install git


### Configuració inicial

> git config --global user.email "you@example.com"

> git config --global user.name "Your Name"

> git config --global push.default simple


### Configuració de claus SSH

> ssh-keygen -o -t rsa -C "ericescriba98@gmail.com" -b 4096

Una vegada hem executat la ordre mostrem el contingut del fitxer id_rsa.pub
per poder obtenir la clau pública i copiar-l'ha al GitLab:

cat ~/.ssh/id_rsa.pub

Exemple:

```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCyt6Ew7rOn9hUx7a3MX13MsdpB+qzF9wwdAD3Hu9Veqg9DLZ2Yl/Vj9x8wzZQA710pcvwLk1hcGI3Vft771TpsO7IWuHE0lNCsLRAcieIMEYwi40nGMGnBkHmYaFsow/bVRw9TwylwnnsQCFIe+c+yg7OsIRLRNT2HV7QRtbnVzeZIpPu3ArFGqlVqxiuVT/vcmXvaMo+/GjKUT66ONMQjNorwPgu4jOfaOLjCoDOE7l1mVXFbO9b+87TBZGhIvC2IYV5rQmIdBdRTllRi80QsD7X85HgkuKWneRtxzaLrIdcM99n0SmW/hH9aCuwt54FeSMr46uNcF2T+V+ICP1K8UOV8df7SGY0ft0muhtE4nQErvDRpgAOkZGU6WQhm0mZXocfuGoxG9wLT8Gwn4cPjZLWGKUCcnM0p42tOTrgY2bT5+hPf44uiDHP1WwOr406v0mj8s+9LtuvFNfeFy5puEGUhKX/sGIyZVccfGPeCI0KaBDrMjwALF/YGlZM4A3dxdoqBpdN6Ut3egloDMuRCPG/7s7l0REmRIGcQBnyHk1vLMHeEMpRPM16rz1cK8FQI8/0NhikE1oWqX1blaacfyEZWhUBG5OP7yka5yBfxbunyVVo7ujATYyStbFoakwJNB4ovwznTtDnBzfMr8BOxen3ROQU0zyQthpQfgGXgMw== ericescriba98@gmail.com
```

### Comprovació de les claus:

Exequtem la següent ordre:

> ssh -T git@gitlab.com

Si ens contesta Welcome to GitLab @isxxxxxxx, és que ha anat correctament.
En cas contrari hauriem d'inciar l'agent ssh amb la següent ordre:

>  $ eval "$(ssh-agent -s)"

> ssh-add

```
Identity added: /home/users/inf/hisx1/isx47983457/.ssh/id_rsa (ericescriba98@gmail.com)
```

### Clonació d'un repositori

> git clone git@gitlab.com:isx47983457/netswithlinux.git

Fem qualsevol canvi en el repositori i ho guardem amb un commit.

Exemple:

> echo "hola" > primerfile.md

> git commit -am "primer git"

I ja podem pujar els canvis al repoitori git al núvol:

> git push


### Afegir canvis del repositori principal al local

Comprovem els repositoris remots que tenim executant-se:

> git remote -v

```
origin	git@gitlab.com:isx47983457/m04-lm-18-19.git (fetch)
origin	git@gitlab.com:isx47983457/m04-lm-18-19.git (push)

```
Per afegir un repositori remot executem la següent ordre:

> git remote add upstream https://gitlab.com/jandugar/m04-lm-18-19.git

> git remote -v

```
origin	git@gitlab.com:isx47983457/m04-lm-18-19.git (fetch)
origin	git@gitlab.com:isx47983457/m04-lm-18-19.git (push)
upstream	https://gitlab.com/jandugar/m04-lm-18-19.git (fetch)
upstream	https://gitlab.com/jandugar/m04-lm-18-19.git (push)

```
> git fetch upstream master


### Actulitzem el nostre repoitori:

> git pull upstream master
