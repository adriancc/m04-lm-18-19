# Eric Escriba
# Septembre 2018
#
# M04
# Script de conversió de codificació de caràcters
######################################################
OK=0
ERR_ARG=1
ERR_TYPE=2


if [ $# -lt 2 -o $# -gt 3 ]
then
    echo "Error: Invalid num of args"
    echo "Usage: convert.sh arg1(from) arg2(to) file"
    exit $ERR_ARG
fi

from=$1
to=$2
file=/dev/stdin

if [ $# -eq 3 ]
then
    file=$3
fi


iconv --list | grep "^$from//$" &> /dev/null

if [ $? -ne 0 ]
then
   echo "Error: $from is an invalid character format"
   echo "Check: iconv --list"
   exit $ERR_TYPE
fi

iconv --list | grep "^$to//$" &> /dev/null

if [ $? -ne 0 ]
then
   echo "Error: $to is an invalid character format"
   echo "Check: iconv --list"
   exit $ERR_TYPE
fi


iconv -f $from -t $to $file

exit $OK

