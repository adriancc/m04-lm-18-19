-- Consulta les pàgines de manual ascii(7), latin1(7), utf8(7).

man 7 ASCII - ASCII character set encoded in octal, decimal, and hexadecimal
man 7 latin1 - iso_8859-1 - ISO 8859-1 character set encoded in octal, decimal, and hexadecimal
man 7 UTF8 - UTF-8 - an ASCII compatible multibyte Unicode encoding

Mostra les formes possibles en les quals es poden codificar amb altres formats


-- Compara el resultat de les ordres man passwd i man 5 passwd.

man passwd - Et mostra les ordres que un usuari pot executar desde el shell
man 5 passwd - Et mostra informacio sobre el fitxer passwd


-- Verifica que entens tots els exemples anteriors de redirecció del shell.

 cmd > output -> reenvia la sortida a un fitxer
 cmd < input -> El fitxer es la entrada estandard
 > fitxer -> Esborra el contingut del fitxer
 cmd > /dev/null 2>&1 -> Envia els missatges d'error i la sortida estandard al /dev/null
 cmd 2>&1 | less ->  Envia els errors a la sortida estandard

-- Converteix un fitxer existent en el sistema a finals de línia propis de Windows.

cp /etc/passwd ./passwd 

unix2dos passwd


-- Troba cadenes de text dins de fitxers binaris.

strings fitxerbinari


-- Converteix la codificació de fitxers de text entre les codificacions UTF-8 i latin1.

iconv -f UTF8 -t latin1 prova.txt

