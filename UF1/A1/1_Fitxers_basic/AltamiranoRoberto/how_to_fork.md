### how to fork by roberto altamirano

# configurem el nostre repository 

lo primer afegin el nostre repository amb el nom perque ho pugui trobar 

> git config --global user.email "you@example.com"
> git config --global user.name "Your Name"
> git config --global push.default simple


# generem las sshkeys 
> ssh-keygen -t rsa -C "rober72004@gmail.com" -b 4096


obtendremos  dos claves, una publica y una privada 

la publica y lo vamos a configurar en el repository en el entorn grafic

estaran en 

.shh/id_rsa.pub 

y la privada 

.shh/id_rsa

apartir de ara puc accedir sense logegarme cada vegada 

> si no funciona tenim aquesta solucio

#start the ssh-agent in the background
$ eval "$(ssh-agent -s)"

$ ssh-add
Enter passphrase for /home/you/.ssh/id_rsa: [tippy tap]
Identity added: /home/you/.ssh/id_rsa (/home/you/.ssh/id_rsa)



# pasem a configurar el repository del profesor per fer un seguiment 

una vegada feta las ssh keys amb la nostra clau pub al directory 
.ssh/id_rsa.pub la posem tambe al entorn grafic

despres comprovem que si fem un pull o un push
ja no demanara la autenticacio del usuari 

despres 

> [isx47262285@i15 m04-lm-18-19]$ git remote -v
> origin	git@gitlab.com:isx47262285/m04-lm-18-19.git (fetch)
> origin	git@gitlab.com:isx47262285/m04-lm-18-19.git (push)


nomes tinguem configurat la nostra branca master 
per afegir el contingut del fork fet del profe, ho em de afegir 
al nostre git remote 

> git remote add upstream https://gitlab.com/jandugar/m04-lm-18-19.git

* apunte el nom "upstream" es perdefecte, es pot posar qualsevol etiqueta 

comprovem 


>[isx47262285@i15 m04-lm-18-19]$ git remote -v
>origin	git@gitlab.com:isx47262285/m04-lm-18-19.git (fetch)
>origin	git@gitlab.com:isx47262285/m04-lm-18-19.git (push)
>upstream	https://gitlab.com/jandugar/m04-lm-18-19.git (fetch)
>upstream	https://gitlab.com/jandugar/m04-lm-18-19.git (push)


en aquest moment tenim el nostre repository fork afegit per tal de fer un
seguiment del repository de profe amb la seguent ordre 

> git pull upstream master 

al navegar pels directoris trobarem que hi ha tot lo que el profesor ha ficat de nou 

# fer el pull request 





