#!/bin/bash
#@edt Sergi Muñoz hisx2
#Date: 24/09/2018
#Desc: Programa que canvia el format del file
#Syn: prog.sh fileor filedest
#############################################
ERR=1
FILERR=2
OK=0
#Validem control arguments
if [ $# -ne 2 ]
then
  echo "ERR: err args"
  echo "USG: prog.sh fileor filedest"
  exit $ERR
fi
#Validem el fitxer
file1=$1
file2=$2
encoding=$(file $file1 | cut -f2 -d' ')
if [ -f $file1 ] 
then
  iconv -f $encoding -t ISO-8859-1 $file1 -o $file2
else
  echo "ERR: file1 not exist"
  echo "USG: prog.sh file1(exist) file2"
  exit $FILERR
fi
exit $OK



