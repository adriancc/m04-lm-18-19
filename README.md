Mòdul Professional 4:\
Llenguatges de marques i sistemes de gestió d’informació
========================================================

Mòdul
:   MP4

Codi BOE
:   0373

Hores mínimes
:   99

Hores de lliure disposició
:   0

Crèdits ECTS
:   7

------------------------------------------------------------------------

**Taula de continguts**

-   [Unitats formatives](#UF)
    -   [Programació amb XML](#MP4UF1)
        -   <div id="MP4UF1RA-TOC">

            </div>

            [Resultats d’aprenentatge i criteris d’avaluació](#MP4UF1RA)
        -   <div id="MP4UF1CO-TOC">

            </div>

            [Continguts](#MP4UF1CO)
        -   <div id="MP4UF1AC-TOC">

            </div>

            [Activitats](#MP4UF1AC)
        -   <div id="MP4UF1CQ-TOC">

            </div>

            [Criteris de qualificació](#MP4UF1CQ)

    -   [Àmbits d’aplicació de l’XML](#MP4UF2)
        -   <div id="MP4UF2RA-TOC">

            </div>

            [Resultats d’aprenentatge i criteris d’avaluació](#MP4UF2RA)
        -   <div id="MP4UF2CO-TOC">

            </div>

            [Continguts](#MP4UF2CO)
        -   <div id="MP4UF2AC-TOC">

            </div>

            [Activitats](#MP4UF2AC)
        -   <div id="MP4UF2CQ-TOC">

            </div>

            [Criteris de qualificació](#MP4UF2CQ)

    -   [Sistemes de gestió empresarial](#MP4UF3)
        -   <div id="MP4UF3RA-TOC">

            </div>

            [Resultats d’aprenentatge i criteris d’avaluació](#MP4UF3RA)
        -   <div id="MP4UF3CO-TOC">

            </div>

            [Continguts](#MP4UF3CO)
        -   <div id="MP4UF3AC-TOC">

            </div>

            [Activitats](#MP4UF3AC)
        -   <div id="MP4UF3CQ-TOC">

            </div>

            [Criteris de qualificació](#MP4UF3CQ)
-   [Criteris de qualificació del mòdul professional](#CA)
-   [Activitats contextualitzades](#TA)

------------------------------------------------------------------------

Unitats formatives {#UF}
------------------

### MP4UF1: Programació amb XML {#MP4UF1}

Hores mínimes
:   45

Hores assignades
:   45

 [Resultats d’aprenentatge i criteris d’avaluació](#MP4UF1RA-TOC) 

:   -   **1.**

        Reconeix les característiques de llenguatges de marques
        analitzant i interpretant fragments de codi.

        -   <div id="MP4UF1RA1CAa">

            </div>

            **1a.**

            Identifica les característiques generals dels llenguatges
            de marques.

        -   <div id="MP4UF1RA1CAb">

            </div>

            **1b.**

            Reconeix els avantatges que proporcionen en el tractament de
            la informació.

        -   <div id="MP4UF1RA1CAc">

            </div>

            **1c.**

            Classifica els llenguatges de marques i identifica els
            més rellevants.

        -   <div id="MP4UF1RA1CAd">

            </div>

            **1d.**

            Diferencia els àmbits d’aplicació dels llenguatges
            de marques.

        -   <div id="MP4UF1RA1CAe">

            </div>

            **1e.**

            Reconeix la necessitat i els àmbits específics d’aplicació
            d’un llenguatge de marques de propòsit general.

        -   <div id="MP4UF1RA1CAf">

            </div>

            **1f.**

            Analitza les característiques pròpies del llenguatge XML.

        -   <div id="MP4UF1RA1CAg">

            </div>

            **1g.**

            Identifica l’estructura d’un document XML i les seves
            regles sintàctiques.

        -   <div id="MP4UF1RA1CAh">

            </div>

            **1h.**

            Contrasta la necessitat de crear documents XML ben formats i
            la influència en el seu processament.

        -   <div id="MP4UF1RA1CAi">

            </div>

            **1i.**

            Identifica els avantatges que aporten els espais de noms.

    -   **2.**

        Utilitza llenguatges de marques per a la transmissió
        d’informació a través del web analitzant l’estructura dels
        documents i identificant els seus elements.

        -   <div id="MP4UF1RA2CAa">

            </div>

            **2a.**

            Identifica i classifica els llenguatges de marques
            relacionats amb la Web i les seves diferents versions.

        -   <div id="MP4UF1RA2CAb">

            </div>

            **2b.**

            Analitza l’estructura d’un document HTML i identifica les
            seccions que el componen.

        -   <div id="MP4UF1RA2CAc">

            </div>

            **2c.**

            Reconeix la funcionalitat de les principals etiquetes i
            atributs del llenguatge HTML.

        -   <div id="MP4UF1RA2CAd">

            </div>

            **2d.**

            Estableix les semblances i diferències entre els llenguatges
            HTML i XHTML.

        -   <div id="MP4UF1RA2CAe">

            </div>

            **2e.**

            Reconeix la utilitat d’XHTML en els sistemes de
            gestió d’informació.

        -   <div id="MP4UF1RA2CAf">

            </div>

            **2f.**

            Utilitza eines en la creació del web.

        -   <div id="MP4UF1RA2CAg">

            </div>

            **2g.**

            Identifica els avantatges que aporta la utilització de
            fulls d’estil.

        -   <div id="MP4UF1RA2CAh">

            </div>

            **2h.**

            Aplica fulls d’estil.

    -   **3.**

        Estableix mecanismes de validació per a documents XML utilitzant
        mètodes per definir la seva sintaxi i estructura.

        -   <div id="MP4UF1RA3CAa">

            </div>

            **3a.**

            Estableix la necessitat de descriure la informació transmesa
            en els documents XML i les seves regles.

        -   <div id="MP4UF1RA3CAb">

            </div>

            **3b.**

            Identifica les tecnologies relacionades amb la definició de
            documents XML.

        -   <div id="MP4UF1RA3CAc">

            </div>

            **3c.**

            Analitza l’estructura i sintaxi específica utilitzada en
            la descripció.

        -   <div id="MP4UF1RA3CAd">

            </div>

            **3d.**

            Crea descripcions de documents XML.

        -   <div id="MP4UF1RA3CAe">

            </div>

            **3e.**

            Utilitza descripcions en l’elaboració i validació de
            documents XML.

        -   <div id="MP4UF1RA3CAf">

            </div>

            **3f.**

            Associa les descripcions de documents XML amb els
            documents XML.

        -   <div id="MP4UF1RA3CAg">

            </div>

            **3g.**

            Utilitza eines específiques de validació.

        -   <div id="MP4UF1RA3CAh">

            </div>

            **3h.**

            Documenta les descripcions de documents XML.

 [Continguts](#MP4UF1CO-TOC) 

:   -   <div id="MP4UF1CO1">

        **1.**
        Reconeixement de les característiques de llenguatges de marques.

        </div>

        -   <div id="MP4UF1CO1.1">

            </div>

            **1.1.**

            Codificació de caràcters\*.

        -   <div id="MP4UF1CO1.2">

            </div>

            **1.2.**

            Classificació.

        -   <div id="MP4UF1CO1.3">

            </div>

            **1.3.**

            XML: estructura i sintaxi.

        -   <div id="MP4UF1CO1.4">

            </div>

            **1.4.**

            Etiquetes.

        -   <div id="MP4UF1CO1.5">

            </div>

            **1.5.**

            Eines d’edició.

        -   <div id="MP4UF1CO1.6">

            </div>

            **1.6.**

            Elaboració de documents XML ben formats.

        -   <div id="MP4UF1CO1.7">

            </div>

            **1.7.**

            Utilització d’espais de noms en XML.

    -   <div id="MP4UF1CO2">

        **2.**
        Utilització de llenguatges de marques en entorns web.

        </div>

        -   <div id="MP4UF1CO2.1">

            </div>

            **2.1.**

            Identificació d’etiquetes i atributs d’HTML.

        -   <div id="MP4UF1CO2.2">

            </div>

            **2.2.**

            XHTML: diferències sintàctiques i estructurals amb HTML.

        -   <div id="MP4UF1CO2.3">

            </div>

            **2.3.**

            Versions de HTML i d’XHTML.

        -   <div id="MP4UF1CO2.4">

            </div>

            **2.4.**

            Eines de disseny web.

        -   <div id="MP4UF1CO2.5">

            </div>

            **2.5.**

            Fulls d’estil.

    -   <div id="MP4UF1CO3">

        **3.**
        Definició d’esquemes i vocabularis en XML.

        </div>

        -   <div id="MP4UF1CO3.1">

            </div>

            **3.1.**

            Utilització de mètodes de definició de documents XML.

        -   <div id="MP4UF1CO3.2">

            </div>

            **3.2.**

            Creació de descripcions.

        -   <div id="MP4UF1CO3.3">

            </div>

            **3.3.**

            Associació amb documents XML.

        -   <div id="MP4UF1CO3.4">

            </div>

            **3.4.**

            Validació.

        -   <div id="MP4UF1CO3.5">

            </div>

            **3.5.**

            Eines de creació i validació.

 [Activitats](#MP4UF1AC-TOC) 

:   +--------------+--------------+--------------+--------------+--------------+--------------+
    | Activitat    | Durada       | RA           | Continguts   | Criteris     | Instruments  |
    |              |              |              |              | d’avaluació  | d’avaluació  |
    +==============+==============+==============+==============+==============+==============+
    | **[A1](#acMP | 12h          | [1](#MP4UF1R | [1](#MP4UF1C | [1a](#MP4UF1 | -   <div     |
    | 4UF1A1 "Acti |              | A1)          | O1)          | RA1CAa),     |     id="MP4U |
    | vitat contex |              |              |              | [1b](#MP4UF1 | F1A1IEPE1">  |
    | tualitzada") |              |              |              | RA1CAb),     |              |
    | **           |              |              |              | [1c](#MP4UF1 |     </div>   |
    | – XML ben    |              |              |              | RA1CAc),     |              |
    | format       |              |              |              | [1d](#MP4UF1 |     Concepte |
    | -   Crear    |              |              |              | RA1CAd),     | s            |
    |     fitxers  |              |              |              | [1e](#MP4UF1 |     fonament |
    |     de text  |              |              |              | RA1CAe),     | als          |
    | -   Editar   |              |              |              | [1f](#MP4UF1 |     dels     |
    |     document |              |              |              | RA1CAf),     |     document |
    | s            |              |              |              | [1g](#MP4UF1 | s            |
    |     XML ben  |              |              |              | RA1CAg),     |     XML      |
    |     formats  |              |              |              | [1h](#MP4UF1 |     ([PE1](# |
    | -   Verifica |              |              |              | RA1CAh)      | QMP4UF1A1IEP |
    | r            |              |              |              | i            | E1))         |
    |     document |              |              |              | [1i](#MP4UF1 | -   <div     |
    | s            |              |              |              | RA1CAi)      |     id="MP4U |
    |     ben      |              |              |              |              | F1A1IEEP1">  |
    |     formats  |              |              |              |              |              |
    | -   Definir  |              |              |              |              |     </div>   |
    |     i        |              |              |              |              |              |
    |     incloure |              |              |              |              |     Creació  |
    |     entitats |              |              |              |              |     de       |
    | -   Combinar |              |              |              |              |     document |
    |     document |              |              |              |              | s            |
    | s            |              |              |              |              |     XML      |
    |     XML amb  |              |              |              |              |     ([EP1](# |
    |     XInclude |              |              |              |              | QMP4UF1A1IEE |
    |              |              |              |              |              | P1))         |
    +--------------+--------------+--------------+--------------+--------------+--------------+
    | **[A2](#acMP | 12h          | [3](#MP4UF1R | [3](#MP4UF1C | [3a](#MP4UF1 | -   <div     |
    | 4UF1A2 "Acti |              | A3)          | O3)          | RA3CAa),     |     id="MP4U |
    | vitat contex |              |              |              | [3b](#MP4UF1 | F1A2IEPE1">  |
    | tualitzada") |              |              |              | RA3CAb),     |              |
    | **           |              |              |              | [3c](#MP4UF1 |     </div>   |
    | – XML vàlid  |              |              |              | RA3CAc),     |              |
    | -   Dissenya |              |              |              | [3d](#MP4UF1 |     Concepte |
    | r            |              |              |              | RA3CAd),     | s            |
    |     nous     |              |              |              | [3e](#MP4UF1 |     bàsics   |
    |     DTDs     |              |              |              | RA3CAe),     |     dels     |
    | -   Validar  |              |              |              | [3f](#MP4UF1 |     DTDs     |
    |     document |              |              |              | RA3CAf),     |     ([PE1](# |
    | s            |              |              |              | [3g](#MP4UF1 | QMP4UF1A2IEP |
    |     XML      |              |              |              | RA3CAg)      | E1))         |
    | -   Organitz |              |              |              | i            | -   <div     |
    | ar           |              |              |              | [3h](#MP4UF1 |     id="MP4U |
    |     i        |              |              |              | RA3CAh)      | F1A2IEEP1">  |
    |     document |              |              |              |              |              |
    | ar           |              |              |              |              |     </div>   |
    |     DTDs     |              |              |              |              |              |
    |              |              |              |              |              |     Creació  |
    |              |              |              |              |              |     de nous  |
    |              |              |              |              |              |     DTDs     |
    |              |              |              |              |              |     ([EP1](# |
    |              |              |              |              |              | QMP4UF1A2IEE |
    |              |              |              |              |              | P1))         |
    +--------------+--------------+--------------+--------------+--------------+--------------+
    | **[A3](#acMP | 21h          | [2](#MP4UF1R | [2](#MP4UF1C | [2a](#MP4UF1 | -   <div     |
    | 4UF1A3 "Acti |              | A2)          | O2)          | RA2CAa),     |     id="MP4U |
    | vitat contex |              |              |              | [2b](#MP4UF1 | F1A3IEPE1">  |
    | tualitzada") |              |              |              | RA2CAb),     |              |
    | **           |              |              |              | [2c](#MP4UF1 |     </div>   |
    | – El         |              |              |              | RA2CAc),     |              |
    | vocabulari   |              |              |              | [2d](#MP4UF1 |     Concepte |
    | HTML 4       |              |              |              | RA2CAd),     | s            |
    | -   Crear i  |              |              |              | [2e](#MP4UF1 |     fonament |
    |     validar  |              |              |              | RA2CAe),     | als          |
    |     document |              |              |              | [2f](#MP4UF1 |     del      |
    | s            |              |              |              | RA2CAf),     |     vocabula |
    |     XHTML    |              |              |              | [2g](#MP4UF1 | ri           |
    | -   Utilitza |              |              |              | RA2CAg)      |     XHTML    |
    | r            |              |              |              | i            |     ([PE1](# |
    |     document |              |              |              | [2h](#MP4UF1 | QMP4UF1A3IEP |
    | ació         |              |              |              | RA2CAh)      | E1))         |
    |     interact |              |              |              |              | -   <div     |
    | iva          |              |              |              |              |     id="MP4U |
    | -   Converti |              |              |              |              | F1A3IEEP1">  |
    | r            |              |              |              |              |              |
    |     document |              |              |              |              |     </div>   |
    | s            |              |              |              |              |              |
    |     HTML a   |              |              |              |              |     Creació  |
    |     XHTML    |              |              |              |              |     de       |
    | -   Editar   |              |              |              |              |     document |
    |     XHTML    |              |              |              |              | s            |
    |     amb un   |              |              |              |              |     XHTML    |
    |     editor   |              |              |              |              |     estricte |
    |     estructu |              |              |              |              |     ([EP1](# |
    | rat          |              |              |              |              | QMP4UF1A3IEE |
    | -   Aplicar  |              |              |              |              | P1))         |
    |     fulls    |              |              |              |              |              |
    |     d’estil  |              |              |              |              |              |
    |     a        |              |              |              |              |              |
    |     document |              |              |              |              |              |
    | s            |              |              |              |              |              |
    |     XHTML    |              |              |              |              |              |
    +--------------+--------------+--------------+--------------+--------------+--------------+

    : [MP4UF1](#MP4UF1): Programació amb XML

 [Criteris de qualificació](#MP4UF1CQ-TOC) 

:   La qualificació de la unitat formativa s’obté a partir de la nota de
    cada instrument d’avaluació i aplicant la següent fórmula:

    <div style="border: 1px solid black; padding: 1em;">

    Q~[UF1](#MP4UF1)~ = 0.33 × Q[~RA1~](#MP4UF1RA1) + 0.33 ×
    Q[~RA2~](#MP4UF1RA2) + 0.34 × Q[~RA3~](#MP4UF1RA3)
    -   Q~[RA1](#MP4UF1RA1)~ = <span id="QMP4UF1A1IEPE1">0.50 ×
        Q[~A1-PE1~](#MP4UF1A1IEPE1)</span> + <span
        id="QMP4UF1A1IEEP1">0.50 × Q[~A1-EP1~](#MP4UF1A1IEEP1)</span> −
        [DR](#DECREMENT)
    -   Q~[RA2](#MP4UF1RA2)~ = <span id="QMP4UF1A3IEPE1">0.50 ×
        Q[~A3-PE1~](#MP4UF1A3IEPE1)</span> + <span
        id="QMP4UF1A3IEEP1">0.50 × Q[~A3-EP1~](#MP4UF1A3IEEP1)</span> −
        [DR](#DECREMENT)
    -   Q~[RA3](#MP4UF1RA3)~ = <span id="QMP4UF1A2IEPE1">0.50 ×
        Q[~A2-PE1~](#MP4UF1A2IEPE1)</span> + <span
        id="QMP4UF1A2IEEP1">0.50 × Q[~A2-EP1~](#MP4UF1A2IEEP1)</span> −
        [DR](#DECREMENT)

    </div>

### MP4UF2: Àmbits d’aplicació de l’XML {#MP4UF2}

Hores mínimes
:   27

Hores assignades
:   27

 [Resultats d’aprenentatge i criteris d’avaluació](#MP4UF2RA-TOC) 

:   -   **1.**

        Genera canals de continguts analitzant i utilitzant tecnologies
        de sindicació.

        -   <div id="MP4UF2RA1CAa">

            </div>

            **1a.**

            Identifica els avantatges que aporta la sindicació de
            continguts en la gestió i transmissió de la informació.

        -   <div id="MP4UF2RA1CAb">

            </div>

            **1b.**

            Defineix els àmbits d’aplicació de la sindicació
            de continguts.

        -   <div id="MP4UF2RA1CAc">

            </div>

            **1c.**

            Analitza les tecnologies en què es basa la sindicació
            de continguts.

        -   <div id="MP4UF2RA1CAd">

            </div>

            **1d.**

            Identifica l’estructura i la sintaxi d’un canal
            de continguts.

        -   <div id="MP4UF2RA1CAe">

            </div>

            **1e.**

            Crea i valida canals de continguts.

        -   <div id="MP4UF2RA1CAf">

            </div>

            **1f.**

            Comprova la funcionalitat i l’accés als canals
            de continguts.

        -   <div id="MP4UF2RA1CAg">

            </div>

            **1g.**

            Utilitza eines específiques com agregadors i directoris
            de canals.

    -   **2.**

        Realitza conversions sobre documents XML utilitzant tècniques i
        eines de processament.

        -   <div id="MP4UF2RA2CAa">

            </div>

            **2a.**

            Identifica la necessitat de la conversió de documents XML.

        -   <div id="MP4UF2RA2CAb">

            </div>

            **2b.**

            Estableix àmbits d’aplicació de la conversió de
            documents XML.

        -   <div id="MP4UF2RA2CAc">

            </div>

            **2c.**

            Analitza les tecnologies implicades i la seva manera
            de funcionament.

        -   <div id="MP4UF2RA2CAd">

            </div>

            **2d.**

            Descriu la sintaxi específica utilitzada en la conversió i
            adaptació de documents XML.

        -   <div id="MP4UF2RA2CAe">

            </div>

            **2e.**

            Crea especificacions de conversió.

        -   <div id="MP4UF2RA2CAf">

            </div>

            **2f.**

            Identifica i caracteritza eines específiques relacionades
            amb la conversió de documents XML.

        -   <div id="MP4UF2RA2CAg">

            </div>

            **2g.**

            Realitza conversions amb diferents formats de sortida.

        -   <div id="MP4UF2RA2CAh">

            </div>

            **2h.**

            Documenta i depura les especificacions de conversió.

    -   **3.**

        Gestiona informació en format XML analitzant i utilitzant
        tecnologies d’emmagatzematge i llenguatges de consulta.

        -   <div id="MP4UF2RA3CAa">

            </div>

            **3a.**

            Identifica els principals mètodes d’emmagatzematge de la
            informació utilitzada en documents XML.

        -   <div id="MP4UF2RA3CAb">

            </div>

            **3b.**

            Identifica els inconvenients d’emmagatzemar informació en
            format XML.

        -   <div id="MP4UF2RA3CAc">

            </div>

            **3c.**

            Estableix tecnologies eficients d’emmagatzematge
            d’informació en funció de les seves característiques.

        -   <div id="MP4UF2RA3CAd">

            </div>

            **3d.**

            ~~Utilitza sistemes gestors de bases de dades relacionals en
            l’emmagatzematge d’informació en format XML~~.

        -   <div id="MP4UF2RA3CAe">

            </div>

            **3e.**

            Utilitza tècniques específiques per crear documents XML a
            partir d’informació emmagatzemada en bases de
            dades relacionals.

        -   <div id="MP4UF2RA3CAf">

            </div>

            **3f.**

            ~~Identifica les característiques dels sistemes gestors de
            bases de dades natives XML~~.

        -   <div id="MP4UF2RA3CAg">

            </div>

            **3g.**

            ~~Instal·la i analitza sistemes gestors de bases de dades
            natives XML~~.

        -   <div id="MP4UF2RA3CAh">

            </div>

            **3h.**

            ~~Utilitza tècniques per gestionar la informació
            emmagatzemada en bases de dades natives XML~~.

        -   <div id="MP4UF2RA3CAi">

            </div>

            **3i.**

            Identifica llenguatges i eines per al tractament i
            emmagatzematge d’informació i la seva inclusió en
            documents XML.

 [Continguts](#MP4UF2CO-TOC) 

:   -   <div id="MP4UF2CO1">

        **1.**
        Aplicació dels llenguatges de marques a la sindicació
        de continguts.

        </div>

        -   <div id="MP4UF2CO1.1">

            </div>

            **1.1.**

            Àmbits d’aplicació.

        -   <div id="MP4UF2CO1.2">

            </div>

            **1.2.**

            Estructura dels canals de continguts.

        -   <div id="MP4UF2CO1.3">

            </div>

            **1.3.**

            Tecnologies de creació de canals de continguts.

        -   <div id="MP4UF2CO1.4">

            </div>

            **1.4.**

            Validació.

        -   <div id="MP4UF2CO1.5">

            </div>

            **1.5.**

            Directoris de canals de continguts.

        -   <div id="MP4UF2CO1.6">

            </div>

            **1.6.**

            Agregació.

    -   <div id="MP4UF2CO2">

        **2.**
        Conversió i adaptació de documents XML.

        </div>

        -   <div id="MP4UF2CO2.1">

            </div>

            **2.1.**

            Tècniques de transformació de documents XML.

        -   <div id="MP4UF2CO2.2">

            </div>

            **2.2.**

            Descripció de l’estructura i de la sintaxi.

        -   <div id="MP4UF2CO2.3">

            </div>

            **2.3.**

            Utilització de plantilles. Utilització d’eines
            de processament.

        -   <div id="MP4UF2CO2.4">

            </div>

            **2.4.**

            Elaboració de documentació.

    -   <div id="MP4UF2CO3">

        **3.**
        Emmagatzematge d’informació.

        </div>

        -   <div id="MP4UF2CO3.1">

            </div>

            **3.1.**

            Sistemes d’emmagatzematge d’informació.

        -   <div id="MP4UF2CO3.2">

            </div>

            **3.2.**

            Inserció i extracció d’informació en XML.

        -   <div id="MP4UF2CO3.3">

            </div>

            **3.3.**

            Tècniques de recerca d’informació en documents XML.

        -   <div id="MP4UF2CO3.4">

            </div>

            **3.4.**

            Llenguatges de consulta i manipulació.

        -   <div id="MP4UF2CO3.5">

            </div>

            **3.5.**

            ~~Dipòsit XML natiu~~.

        -   <div id="MP4UF2CO3.6">

            </div>

            **3.6.**

            Eines de tractament i emmagatzematge d’informació en
            format XML.

 [Activitats](#MP4UF2AC-TOC) 

:   +--------------+--------------+--------------+--------------+--------------+--------------+
    | Activitat    | Durada       | RA           | Continguts   | Criteris     | Instruments  |
    |              |              |              |              | d’avaluació  | d’avaluació  |
    +==============+==============+==============+==============+==============+==============+
    | **[A1](#acMP | 6h           | [1](#MP4UF2R | [1](#MP4UF2C | [1a](#MP4UF2 | -   <div     |
    | 4UF2A1 "Acti |              | A1)          | O1)          | RA1CAa),     |     id="MP4U |
    | vitat contex |              |              |              | [1b](#MP4UF2 | F2A1IEPE1">  |
    | tualitzada") |              |              |              | RA1CAb),     |              |
    | **           |              |              |              | [1c](#MP4UF2 |     </div>   |
    | – La família |              |              |              | RA1CAc),     |              |
    | de           |              |              |              | [1d](#MP4UF2 |     Concepte |
    | vocabularis  |              |              |              | RA1CAd),     | s            |
    | RSS          |              |              |              | [1e](#MP4UF2 |     fonament |
    | -   Utilitza |              |              |              | RA1CAe),     | als          |
    | r            |              |              |              | [1f](#MP4UF2 |     del      |
    |     lectors  |              |              |              | RA1CAf)      |     vocabula |
    |     i        |              |              |              | i            | ri           |
    |     agregado |              |              |              | [1g](#MP4UF2 |     RSS      |
    | rs           |              |              |              | RA1CAg)      |     ([PE1](# |
    |     RSS      |              |              |              |              | QMP4UF2A1IEP |
    | -   Crear i  |              |              |              |              | E1))         |
    |     validar  |              |              |              |              | -   <div     |
    |     document |              |              |              |              |     id="MP4U |
    | s            |              |              |              |              | F2A1IEEP1">  |
    |     RSS      |              |              |              |              |              |
    | -   Aplicar  |              |              |              |              |     </div>   |
    |     fulls    |              |              |              |              |              |
    |     d’estil  |              |              |              |              |     Creació  |
    |     a        |              |              |              |              |     i        |
    |     document |              |              |              |              |     validaci |
    | s            |              |              |              |              | ó            |
    |     RSS      |              |              |              |              |     de       |
    |              |              |              |              |              |     document |
    |              |              |              |              |              | s            |
    |              |              |              |              |              |     RSS      |
    |              |              |              |              |              |     ([EP1](# |
    |              |              |              |              |              | QMP4UF2A1IEE |
    |              |              |              |              |              | P1))         |
    +--------------+--------------+--------------+--------------+--------------+--------------+
    | **[A2](#acMP | 12h          | [3](#MP4UF2R | [3](#MP4UF2C | [3a](#MP4UF2 | -   <div     |
    | 4UF2A2 "Acti |              | A3)          | O3)          | RA3CAa),     |     id="MP4U |
    | vitat contex |              |              |              | [3b](#MP4UF2 | F2A2IEPE1">  |
    | tualitzada") |              |              |              | RA3CAb),     |              |
    | **           |              |              |              | [3c](#MP4UF2 |     </div>   |
    | – Sistemes   |              |              |              | RA3CAc),     |              |
    | de consulta  |              |              |              | [3e](#MP4UF2 |     Tècnique |
    | i            |              |              |              | RA3CAe)      | s            |
    | emmagatzemat |              |              |              | i            |     de       |
    | ge           |              |              |              | [3i](#MP4UF2 |     programa |
    | de documents |              |              |              | RA3CAi)      | ció          |
    | XML          |              |              |              |              |     declarat |
    | -   Consulta |              |              |              |              | iva          |
    | r            |              |              |              |              |     amb      |
    |     document |              |              |              |              |     Python   |
    | s            |              |              |              |              |     ([PE1](# |
    |     XML      |              |              |              |              | QMP4UF2A2IEP |
    | -   Comprimi |              |              |              |              | E1))         |
    | r            |              |              |              |              | -   <div     |
    |     document |              |              |              |              |     id="MP4U |
    | s            |              |              |              |              | F2A2IEEP1">  |
    |     XML      |              |              |              |              |              |
    | -   Extreure |              |              |              |              |     </div>   |
    |     dades de |              |              |              |              |              |
    |     SGBD     |              |              |              |              |     Interrog |
    |     relacion |              |              |              |              | ació         |
    | als          |              |              |              |              |     de       |
    |     en       |              |              |              |              |     document |
    |     format   |              |              |              |              | s            |
    |     XML      |              |              |              |              |     XML      |
    |              |              |              |              |              |     ([EP1](# |
    |              |              |              |              |              | QMP4UF2A2IEE |
    |              |              |              |              |              | P1))         |
    +--------------+--------------+--------------+--------------+--------------+--------------+
    | **[A3](#acMP | 9h           | [2](#MP4UF2R | [2](#MP4UF2C | [2a](#MP4UF2 | -   <div     |
    | 4UF2A3 "Acti |              | A2)          | O2)          | RA2CAa),     |     id="MP4U |
    | vitat contex |              |              |              | [2b](#MP4UF2 | F2A3IEPE1">  |
    | tualitzada") |              |              |              | RA2CAb),     |              |
    | **           |              |              |              | [2c](#MP4UF2 |     </div>   |
    | –            |              |              |              | RA2CAc),     |              |
    | Transformaci |              |              |              | [2d](#MP4UF2 |     Caracter |
    | ó            |              |              |              | RA2CAd),     | ístiques     |
    | de documents |              |              |              | [2e](#MP4UF2 |     del      |
    | XML          |              |              |              | RA2CAe),     |     mòdul    |
    | -   Transfor |              |              |              | [2f](#MP4UF2 |     ElementT |
    | mar          |              |              |              | RA2CAf),     | ree          |
    |     document |              |              |              | [2g](#MP4UF2 |     de       |
    | s            |              |              |              | RA2CAg)      |     Python   |
    |     XML      |              |              |              | i            |     ([PE1](# |
    | -   Organitz |              |              |              | [2h](#MP4UF2 | QMP4UF2A3IEP |
    | ar           |              |              |              | RA2CAh)      | E1))         |
    |     i        |              |              |              |              | -   <div     |
    |     document |              |              |              |              |     id="MP4U |
    | ar           |              |              |              |              | F2A3IEEP1">  |
    |     les      |              |              |              |              |              |
    |     transfor |              |              |              |              |     </div>   |
    | macions      |              |              |              |              |              |
    |              |              |              |              |              |     Programa |
    |              |              |              |              |              | ció          |
    |              |              |              |              |              |     de       |
    |              |              |              |              |              |     transfor |
    |              |              |              |              |              | macions      |
    |              |              |              |              |              |     ([EP1](# |
    |              |              |              |              |              | QMP4UF2A3IEE |
    |              |              |              |              |              | P1))         |
    +--------------+--------------+--------------+--------------+--------------+--------------+

    : [MP4UF2](#MP4UF2): Àmbits d’aplicació de l’XML

 [Criteris de qualificació](#MP4UF2CQ-TOC) 

:   La qualificació de la unitat formativa s’obté a partir de la nota de
    cada instrument d’avaluació i aplicant la següent fórmula:

    <div style="border: 1px solid black; padding: 1em;">

    Q~[UF2](#MP4UF2)~ = 0.20 × Q[~RA1~](#MP4UF2RA1) + 0.40 ×
    Q[~RA2~](#MP4UF2RA2) + 0.40 × Q[~RA3~](#MP4UF2RA3)
    -   Q~[RA1](#MP4UF2RA1)~ = <span id="QMP4UF2A1IEPE1">0.50 ×
        Q[~A1-PE1~](#MP4UF2A1IEPE1)</span> + <span
        id="QMP4UF2A1IEEP1">0.50 × Q[~A1-EP1~](#MP4UF2A1IEEP1)</span> −
        [DR](#DECREMENT)
    -   Q~[RA2](#MP4UF2RA2)~ = <span id="QMP4UF2A3IEPE1">0.50 ×
        Q[~A3-PE1~](#MP4UF2A3IEPE1)</span> + <span
        id="QMP4UF2A3IEEP1">0.50 × Q[~A3-EP1~](#MP4UF2A3IEEP1)</span> −
        [DR](#DECREMENT)
    -   Q~[RA3](#MP4UF2RA3)~ = <span id="QMP4UF2A2IEPE1">0.50 ×
        Q[~A2-PE1~](#MP4UF2A2IEPE1)</span> + <span
        id="QMP4UF2A2IEEP1">0.50 × Q[~A2-EP1~](#MP4UF2A2IEEP1)</span> −
        [DR](#DECREMENT)

    </div>

### MP4UF3: Sistemes de gestió empresarial {#MP4UF3}

Hores mínimes
:   27

Hores assignades
:   27

 [Resultats d’aprenentatge i criteris d’avaluació](#MP4UF3RA-TOC) 

:   -   **1.**

        Treballa amb sistemes empresarials de gestió d’informació
        realitzant tasques d’importació, integració, assegurament i
        extracció de la informació.

        -   <div id="MP4UF3RA1CAa">

            </div>

            **1a.**

            Reconeix els avantatges dels sistemes de gestió i
            planificació de recursos empresarials.

        -   <div id="MP4UF3RA1CAb">

            </div>

            **1b.**

            ~~Avalua les característiques de les principals aplicacions
            de gestió empresarial~~.

        -   <div id="MP4UF3RA1CAc">

            </div>

            **1c.**

            Instal·la aplicacions de gestió empresarial.

        -   <div id="MP4UF3RA1CAd">

            </div>

            **1d.**

            ~~Configura i adapta les aplicacions~~.

        -   <div id="MP4UF3RA1CAe">

            </div>

            **1e.**

            Estableix i verifica l’accés segur a la informació.

        -   <div id="MP4UF3RA1CAf">

            </div>

            **1f.**

            Genera informes.

        -   <div id="MP4UF3RA1CAg">

            </div>

            **1g.**

            Realitza tasques d’integració amb aplicacions ofimàtiques.

        -   <div id="MP4UF3RA1CAh">

            </div>

            **1h.**

            Realitza procediments d’extracció d’informació per al seu
            tractament i incorporació a diversos sistemes.

        -   <div id="MP4UF3RA1CAi">

            </div>

            **1i.**

            ~~Realitza tasques d’assistència i resolució
            d’incidències~~.

        -   <div id="MP4UF3RA1CAj">

            </div>

            **1j.**

            ~~Elabora documents relatius a l’explotació de
            l’aplicació~~.

 [Continguts](#MP4UF3CO-TOC) 

:   -   <div id="MP4UF3CO1">

        **1.**
        Sistemes de gestió empresarial.

        </div>

        -   <div id="MP4UF3CO1.1">

            </div>

            **1.1.**

            Instal·lació.

        -   <div id="MP4UF3CO1.2">

            </div>

            **1.2.**

            ~~Adaptació i configuració~~.

        -   <div id="MP4UF3CO1.3">

            </div>

            **1.3.**

            ~~Integració de mòduls~~.

        -   <div id="MP4UF3CO1.4">

            </div>

            **1.4.**

            Elaboració d’informes.

        -   <div id="MP4UF3CO1.5">

            </div>

            **1.5.**

            Integració amb aplicacions ofimàtiques.

        -   <div id="MP4UF3CO1.6">

            </div>

            **1.6.**

            Exportació d’informació.

 [Activitats](#MP4UF3AC-TOC) 

:   +--------------+--------------+--------------+--------------+--------------+--------------+
    | Activitat    | Durada       | RA           | Continguts   | Criteris     | Instruments  |
    |              |              |              |              | d’avaluació  | d’avaluació  |
    +==============+==============+==============+==============+==============+==============+
    | **[A1](#acMP | 27h          | [1](#MP4UF3R | [1](#MP4UF3C | [1a](#MP4UF3 | -   <div     |
    | 4UF3A1 "Acti |              | A1)          | O1)          | RA1CAa),     |     id="MP4U |
    | vitat contex |              |              |              | [1c](#MP4UF3 | F3A1IEPE1">  |
    | tualitzada") |              |              |              | RA1CAc),     |              |
    | **           |              |              |              | [1e](#MP4UF3 |     </div>   |
    | – Formularis |              |              |              | RA1CAe),     |              |
    | HTML         |              |              |              | [1f](#MP4UF3 |     Concepte |
    | interactius  |              |              |              | RA1CAf),     | s            |
    | -   Crear    |              |              |              | [1g](#MP4UF3 |     bàsics   |
    |     formular |              |              |              | RA1CAg)      |     dels     |
    | is           |              |              |              | i            |     formular |
    | -   Presenta |              |              |              | [1h](#MP4UF3 | is           |
    | r            |              |              |              | RA1CAh)      |     HTML     |
    |     formular |              |              |              |              |     ([PE1](# |
    | is           |              |              |              |              | QMP4UF3A1IEP |
    |     i dades  |              |              |              |              | E1))         |
    |     utilitza |              |              |              |              | -   <div     |
    | nt           |              |              |              |              |     id="MP4U |
    |     taules   |              |              |              |              | F3A1IEEP1">  |
    | -   Processa |              |              |              |              |              |
    | r            |              |              |              |              |     </div>   |
    |     formular |              |              |              |              |              |
    | is           |              |              |              |              |     Creació  |
    |              |              |              |              |              |     de       |
    |              |              |              |              |              |     formular |
    |              |              |              |              |              | is           |
    |              |              |              |              |              |     ([EP1](# |
    |              |              |              |              |              | QMP4UF3A1IEE |
    |              |              |              |              |              | P1))         |
    |              |              |              |              |              | -   <div     |
    |              |              |              |              |              |     id="MP4U |
    |              |              |              |              |              | F3A1IEEP2">  |
    |              |              |              |              |              |              |
    |              |              |              |              |              |     </div>   |
    |              |              |              |              |              |              |
    |              |              |              |              |              |     Processa |
    |              |              |              |              |              | ment         |
    |              |              |              |              |              |     de       |
    |              |              |              |              |              |     formular |
    |              |              |              |              |              | is           |
    |              |              |              |              |              |     ([EP2](# |
    |              |              |              |              |              | QMP4UF3A1IEE |
    |              |              |              |              |              | P2))         |
    +--------------+--------------+--------------+--------------+--------------+--------------+

    : [MP4UF3](#MP4UF3): Sistemes de gestió empresarial

 [Criteris de qualificació](#MP4UF3CQ-TOC) 

:   La qualificació de la unitat formativa s’obté a partir de la nota de
    cada instrument d’avaluació i aplicant la següent fórmula:

    <div style="border: 1px solid black; padding: 1em;">

    Q~[UF3](#MP4UF3)~ = 1.00 × Q[~RA1~](#MP4UF3RA1)
    -   Q~[RA1](#MP4UF3RA1)~ = <span id="QMP4UF3A1IEPE1">0.34 ×
        Q[~A1-PE1~](#MP4UF3A1IEPE1)</span> + <span
        id="QMP4UF3A1IEEP1">0.33 × Q[~A1-EP1~](#MP4UF3A1IEEP1)</span> +
        <span id="QMP4UF3A1IEEP2">0.33 ×
        Q[~A1-EP2~](#MP4UF3A1IEEP2)</span> − [DR](#DECREMENT)

    </div>

------------------------------------------------------------------------

Criteris de qualificació del mòdul professional {#CA}
-----------------------------------------------

La qualificació del mòdul professional s’obté a partir de la nota de
cada unitat formativa i aplicant la següent fórmula:

-   Q~[MP4](#)~ = 0.45 × Q[~UF1~](#MP4UF1) + 0.27 × Q[~UF2~](#MP4UF2) +
    0.27 × Q[~UF3~](#MP4UF3)
    -   Q~[UF1](#MP4UF1)~ = 0.33 × Q[~RA1~](#MP4UF1RA1) + 0.33 ×
        Q[~RA2~](#MP4UF1RA2) + 0.34 × Q[~RA3~](#MP4UF1RA3)
        -   Q~[RA1](#MP4UF1RA1)~ = <span>0.50 ×
            Q[~A1-PE1~](#MP4UF1A1IEPE1)</span> + <span>0.50 ×
            Q[~A1-EP1~](#MP4UF1A1IEEP1)</span> − [DR](#DECREMENT)
        -   Q~[RA2](#MP4UF1RA2)~ = <span>0.50 ×
            Q[~A3-PE1~](#MP4UF1A3IEPE1)</span> + <span>0.50 ×
            Q[~A3-EP1~](#MP4UF1A3IEEP1)</span> − [DR](#DECREMENT)
        -   Q~[RA3](#MP4UF1RA3)~ = <span>0.50 ×
            Q[~A2-PE1~](#MP4UF1A2IEPE1)</span> + <span>0.50 ×
            Q[~A2-EP1~](#MP4UF1A2IEEP1)</span> − [DR](#DECREMENT)
        -   <span>TR: 0 (0.00%)   </span><span>PR: 0
            (0.00%)   </span><span>PE: 3 (50.00%)   </span><span>EP: 3
            (50.00%)   </span>
    -   Q~[UF2](#MP4UF2)~ = 0.20 × Q[~RA1~](#MP4UF2RA1) + 0.40 ×
        Q[~RA2~](#MP4UF2RA2) + 0.40 × Q[~RA3~](#MP4UF2RA3)
        -   Q~[RA1](#MP4UF2RA1)~ = <span>0.50 ×
            Q[~A1-PE1~](#MP4UF2A1IEPE1)</span> + <span>0.50 ×
            Q[~A1-EP1~](#MP4UF2A1IEEP1)</span> − [DR](#DECREMENT)
        -   Q~[RA2](#MP4UF2RA2)~ = <span>0.50 ×
            Q[~A3-PE1~](#MP4UF2A3IEPE1)</span> + <span>0.50 ×
            Q[~A3-EP1~](#MP4UF2A3IEEP1)</span> − [DR](#DECREMENT)
        -   Q~[RA3](#MP4UF2RA3)~ = <span>0.50 ×
            Q[~A2-PE1~](#MP4UF2A2IEPE1)</span> + <span>0.50 ×
            Q[~A2-EP1~](#MP4UF2A2IEEP1)</span> − [DR](#DECREMENT)
        -   <span>TR: 0 (0.00%)   </span><span>PR: 0
            (0.00%)   </span><span>PE: 3 (50.00%)   </span><span>EP: 3
            (50.00%)   </span>
    -   Q~[UF3](#MP4UF3)~ = 1.00 × Q[~RA1~](#MP4UF3RA1)
        -   Q~[RA1](#MP4UF3RA1)~ = <span>0.34 ×
            Q[~A1-PE1~](#MP4UF3A1IEPE1)</span> + <span>0.33 ×
            Q[~A1-EP1~](#MP4UF3A1IEEP1)</span> + <span>0.33 ×
            Q[~A1-EP2~](#MP4UF3A1IEEP2)</span> − [DR](#DECREMENT)
        -   <span>TR: 0 (0.00%)   </span><span>PR: 0
            (0.00%)   </span><span>PE: 1 (34.00%)   </span><span>EP: 2
            (66.00%)   </span>

Q
:   Qualificació

MP
:   Mòdul professional

UF
:   Unitat formativa

A
:   Activitat

RA
:   Resultat d’aprenentatge

EP
:   Exercici pràctic

PE
:   Prova escrita

PR
:   Projecte

TR
:   Treball

GO
:   Graella d’observacions

DR (decrement per resultat d’aprenentatge)
:   Decrement per cada RA en base a les faltes d’assistència i treball
    incorrecte segons aquest barem:
    -   Faltes d'assistència superiors al 25%: pèrdua del dret
        a avaluació.
    -   Faltes d'assistència per sota del 25%: reducció proporcional en
        la nota; per exemple, una nota en el RA de 10 amb un 20% de
        faltes dóna lloc a una nota final de 8.
    -   Reiterades negatives a emprar les metodologies de treball
        suggerides: fins a un 20% de reducció en la nota.

    Les diverses causes de decrement es poden combinar.

### Sistema de recuperació

En cas de no superar la unitat formativa de forma continua, es
realitzarà una prova en el període de convocatòria extraordinària.
Aquesta prova constarà d’una part escrita i una part pràctica. Requisit
necessari per presentar-se a la prova serà aportar per escrit totes les
proves escrites i treballs realitzats al llarg de la unitat formativa.
La nota resultat de realitzar la prova serà *aprovat* o *no aprovat*.

------------------------------------------------------------------------

Activitats contextualitzades {#TA}
----------------------------

### [UF1](#MP4UF1): Programació amb XML

[Activitat 1](#qaMP4UF1A1 "Quadre d’activitats"): XML ben format (12
hores)

Resultats d’aprenentatge i criteris d'avaluació

Continguts

Tasques

**1.** Reconeix les característiques de llenguatges de marques
analitzant i interpretant fragments de codi

-   **a.**

    Identifica les característiques generals dels llenguatges de marques

-   **b.**

    Reconeix els avantatges que proporcionen en el tractament de la
    informació

-   **c.**

    Classifica els llenguatges de marques i identifica els més
    rellevants

-   **d.**

    Diferencia els àmbits d’aplicació dels llenguatges de marques

-   **e.**

    Reconeix la necessitat i els àmbits específics d’aplicació d’un
    llenguatge de marques de propòsit general

-   **f.**

    Analitza les característiques pròpies del llenguatge XML

-   **g.**

    Identifica l’estructura d’un document XML i les seves regles
    sintàctiques

-   **h.**

    Contrasta la necessitat de crear documents XML ben formats i la
    influència en el seu processament

-   **i.**

    Identifica els avantatges que aporten els espais de noms

**1.** Reconeixement de les característiques de llenguatges de marques

-   **1.1.**

    Codificació de caràcters\*.

-   **1.2.**

    Classificació.

-   **1.3.**

    XML: estructura i sintaxi.

-   **1.4.**

    Etiquetes.

-   **1.5.**

    Eines d’edició.

-   **1.6.**

    Elaboració de documents XML ben formats.

-   **1.7.**

    Utilització d’espais de noms en XML.

XML ben format

-   Crear fitxers de text
-   Editar documents XML ben formats
-   Verificar documents ben formats
-   Definir i incloure entitats
-   Combinar documents XML amb XInclude

Instruments d’avaluació

-   Conceptes fonamentals dels documents XML (PE1)
-   Creació de documents XML (EP1)

[Activitat 2](#qaMP4UF1A2 "Quadre d’activitats"): XML vàlid (12 hores)

Resultats d’aprenentatge i criteris d'avaluació

Continguts

Tasques

**3.** Estableix mecanismes de validació per a documents XML utilitzant
mètodes per definir la seva sintaxi i estructura

-   **a.**

    Estableix la necessitat de descriure la informació transmesa en els
    documents XML i les seves regles

-   **b.**

    Identifica les tecnologies relacionades amb la definició de
    documents XML

-   **c.**

    Analitza l’estructura i sintaxi específica utilitzada en la
    descripció

-   **d.**

    Crea descripcions de documents XML

-   **e.**

    Utilitza descripcions en l’elaboració i validació de documents XML

-   **f.**

    Associa les descripcions de documents XML amb els documents XML

-   **g.**

    Utilitza eines específiques de validació

-   **h.**

    Documenta les descripcions de documents XML

**3.** Definició d’esquemes i vocabularis en XML

-   **3.1.**

    Utilització de mètodes de definició de documents XML.

-   **3.2.**

    Creació de descripcions.

-   **3.3.**

    Associació amb documents XML.

-   **3.4.**

    Validació.

-   **3.5.**

    Eines de creació i validació.

XML vàlid

-   Dissenyar nous DTDs
-   Validar documents XML
-   Organitzar i documentar DTDs

Instruments d’avaluació

-   Conceptes bàsics dels DTDs (PE1)
-   Creació de nous DTDs (EP1)

[Activitat 3](#qaMP4UF1A3 "Quadre d’activitats"): El vocabulari HTML 4
(21 hores)

Resultats d’aprenentatge i criteris d'avaluació

Continguts

Tasques

**2.** Utilitza llenguatges de marques per a la transmissió d’informació
a través del web analitzant l’estructura dels documents i identificant
els seus elements

-   **a.**

    Identifica i classifica els llenguatges de marques relacionats amb
    la Web i les seves diferents versions

-   **b.**

    Analitza l’estructura d’un document HTML i identifica les seccions
    que el componen

-   **c.**

    Reconeix la funcionalitat de les principals etiquetes i atributs del
    llenguatge HTML

-   **d.**

    Estableix les semblances i diferències entre els llenguatges HTML i
    XHTML

-   **e.**

    Reconeix la utilitat d’XHTML en els sistemes de gestió d’informació

-   **f.**

    Utilitza eines en la creació del web

-   **g.**

    Identifica els avantatges que aporta la utilització de fulls d’estil

-   **h.**

    Aplica fulls d’estil

**2.** Utilització de llenguatges de marques en entorns web

-   **2.1.**

    Identificació d’etiquetes i atributs d’HTML.

-   **2.2.**

    XHTML: diferències sintàctiques i estructurals amb HTML.

-   **2.3.**

    Versions de HTML i d’XHTML.

-   **2.4.**

    Eines de disseny web.

-   **2.5.**

    Fulls d’estil.

El vocabulari HTML 4

-   Crear i validar documents XHTML
-   Utilitzar documentació interactiva
-   Convertir documents HTML a XHTML
-   Editar XHTML amb un editor estructurat
-   Aplicar fulls d’estil a documents XHTML

Instruments d’avaluació

-   Conceptes fonamentals del vocabulari XHTML (PE1)
-   Creació de documents XHTML estricte (EP1)

### [UF2](#MP4UF2): Àmbits d’aplicació de l’XML

[Activitat 1](#qaMP4UF2A1 "Quadre d’activitats"): La família de
vocabularis RSS (6 hores)

Resultats d’aprenentatge i criteris d'avaluació

Continguts

Tasques

**1.** Genera canals de continguts analitzant i utilitzant tecnologies
de sindicació

-   **a.**

    Identifica els avantatges que aporta la sindicació de continguts en
    la gestió i transmissió de la informació

-   **b.**

    Defineix els àmbits d’aplicació de la sindicació de continguts

-   **c.**

    Analitza les tecnologies en què es basa la sindicació de continguts

-   **d.**

    Identifica l’estructura i la sintaxi d’un canal de continguts

-   **e.**

    Crea i valida canals de continguts

-   **f.**

    Comprova la funcionalitat i l’accés als canals de continguts

-   **g.**

    Utilitza eines específiques com agregadors i directoris de canals

**1.** Aplicació dels llenguatges de marques a la sindicació de
continguts

-   **1.1.**

    Àmbits d’aplicació.

-   **1.2.**

    Estructura dels canals de continguts.

-   **1.3.**

    Tecnologies de creació de canals de continguts.

-   **1.4.**

    Validació.

-   **1.5.**

    Directoris de canals de continguts.

-   **1.6.**

    Agregació.

La família de vocabularis RSS

-   Utilitzar lectors i agregadors RSS
-   Crear i validar documents RSS
-   Aplicar fulls d’estil a documents RSS

Instruments d’avaluació

-   Conceptes fonamentals del vocabulari RSS (PE1)
-   Creació i validació de documents RSS (EP1)

[Activitat 2](#qaMP4UF2A2 "Quadre d’activitats"): Sistemes de consulta i
emmagatzematge de documents XML (12 hores)

Resultats d’aprenentatge i criteris d'avaluació

Continguts

Tasques

**3.** Gestiona informació en format XML analitzant i utilitzant
tecnologies d’emmagatzematge i llenguatges de consulta

-   **a.**

    Identifica els principals mètodes d’emmagatzematge de la informació
    utilitzada en documents XML

-   **b.**

    Identifica els inconvenients d’emmagatzemar informació en format XML

-   **c.**

    Estableix tecnologies eficients d’emmagatzematge d’informació en
    funció de les seves característiques

-   **e.**

    Utilitza tècniques específiques per crear documents XML a partir
    d’informació emmagatzemada en bases de dades relacionals

-   **i.**

    Identifica llenguatges i eines per al tractament i emmagatzematge
    d’informació i la seva inclusió en documents XML

**3.** Emmagatzematge d’informació

-   **3.1.**

    Sistemes d’emmagatzematge d’informació.

-   **3.2.**

    Inserció i extracció d’informació en XML.

-   **3.3.**

    Tècniques de recerca d’informació en documents XML.

-   **3.4.**

    Llenguatges de consulta i manipulació.

-   **3.6.**

    Eines de tractament i emmagatzematge d’informació en format XML.

Sistemes de consulta i emmagatzematge de documents XML

-   Consultar documents XML
-   Comprimir documents XML
-   Extreure dades de SGBD relacionals en format XML

Instruments d’avaluació

-   Tècniques de programació declarativa amb Python (PE1)
-   Interrogació de documents XML (EP1)

[Activitat 3](#qaMP4UF2A3 "Quadre d’activitats"): Transformació de
documents XML (9 hores)

Resultats d’aprenentatge i criteris d'avaluació

Continguts

Tasques

**2.** Realitza conversions sobre documents XML utilitzant tècniques i
eines de processament

-   **a.**

    Identifica la necessitat de la conversió de documents XML

-   **b.**

    Estableix àmbits d’aplicació de la conversió de documents XML

-   **c.**

    Analitza les tecnologies implicades i la seva manera de funcionament

-   **d.**

    Descriu la sintaxi específica utilitzada en la conversió i adaptació
    de documents XML

-   **e.**

    Crea especificacions de conversió

-   **f.**

    Identifica i caracteritza eines específiques relacionades amb la
    conversió de documents XML

-   **g.**

    Realitza conversions amb diferents formats de sortida

-   **h.**

    Documenta i depura les especificacions de conversió

**2.** Conversió i adaptació de documents XML

-   **2.1.**

    Tècniques de transformació de documents XML.

-   **2.2.**

    Descripció de l’estructura i de la sintaxi.

-   **2.3.**

    Utilització de plantilles. Utilització d’eines de processament.

-   **2.4.**

    Elaboració de documentació.

Transformació de documents XML

-   Transformar documents XML
-   Organitzar i documentar les transformacions

Instruments d’avaluació

-   Característiques del mòdul ElementTree de Python (PE1)
-   Programació de transformacions (EP1)

### [UF3](#MP4UF3): Sistemes de gestió empresarial

[Activitat 1](#qaMP4UF3A1 "Quadre d’activitats"): Formularis HTML
interactius (27 hores)

Resultats d’aprenentatge i criteris d'avaluació

Continguts

Tasques

**1.** Treballa amb sistemes empresarials de gestió d’informació
realitzant tasques d’importació, integració, assegurament i extracció de
la informació

-   **a.**

    Reconeix els avantatges dels sistemes de gestió i planificació de
    recursos empresarials

-   **c.**

    Instal·la aplicacions de gestió empresarial

-   **e.**

    Estableix i verifica l’accés segur a la informació

-   **f.**

    Genera informes

-   **g.**

    Realitza tasques d’integració amb aplicacions ofimàtiques

-   **h.**

    Realitza procediments d’extracció d’informació per al seu tractament
    i incorporació a diversos sistemes

**1.** Sistemes de gestió empresarial

-   **1.1.**

    Instal·lació.

-   **1.4.**

    Elaboració d’informes.

-   **1.5.**

    Integració amb aplicacions ofimàtiques.

-   **1.6.**

    Exportació d’informació.

Formularis HTML interactius

-   Crear formularis
-   Presentar formularis i dades utilitzant taules
-   Processar formularis

Instruments d’avaluació

-   Conceptes bàsics dels formularis HTML (PE1)
-   Creació de formularis (EP1)
-   Processament de formularis (EP2)

